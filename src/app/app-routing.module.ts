import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ProductIndexComponent} from "./product/product-index/product-index.component";
import {ProductCreateComponent} from "./product/product-create/product-create.component";

const routes: Routes = [
  { path: 'product', component: ProductIndexComponent},
  { path: 'product/create', component: ProductCreateComponent},
  { path: 'product/edit/:id', component: ProductCreateComponent},
  { path: 'product/view/:id', component: ProductCreateComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
