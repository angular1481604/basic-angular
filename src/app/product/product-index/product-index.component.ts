import {Component, OnInit} from '@angular/core';
import {ProductType} from "../../share/types/product.type";

@Component({
  selector: 'app-product-index',
  templateUrl: './product-index.component.html',
  styleUrls: ['./product-index.component.css']
})
export class ProductIndexComponent implements OnInit {
  productList: ProductType[] = [];

  constructor() {
  }

  ngOnInit(): void {
    this.loadData();
  }

  loadData(): void {
    this.productList = [
      {id: 1, name: 'layan', quantity: 1, description: 'lilou play'},
      {id: 2, name: 'ameni', quantity: 1, description: 'ameni play'},
      {id: 3, name: 'riadh', quantity: 1, description: 'riadh play'}
    ]
  }
}
