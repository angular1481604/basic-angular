import {Component, Input, OnInit} from '@angular/core';
import {ProductType} from "../../share/types/product.type";

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
  @Input() products: ProductType[] = []; // decorate the property with @Input()

  constructor() {
  }

  ngOnInit(): void {
  }

}
