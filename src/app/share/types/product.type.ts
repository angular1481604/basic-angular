export interface ProductType {
  'id': number;
  'name': string;
  'quantity': number;
  'description'?: string;
}
