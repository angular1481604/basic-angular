import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  private _productUrl = 'product';

  get productUrl(): string {
    return this._productUrl;
  }

  set productUrl(value: string) {
    this._productUrl = value;
  }

  constructor() {
  }

  ngOnInit(): void {
  }

}
